package me.kingjan1999;

import view.*;
import view.visitor.DocumentArchiveVisitor;
import view.visitor.ModuleOrUnitDirectVisitor;
import viewClient.ConnectionIndex;
import viewClient.ExceptionAndEventHandler;
import viewClient.ServerConnection;
import viewClient.StudentServiceConnection;

import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

public class NotenApi {

    private String username;
    private String password;
    private ExceptionAndEventHandler reporter;
    private MarkProcessor processor;
    private ServerConnection con;

    private StudentView stud;
    private StudentServiceView ssv;

    protected String programTitle, stats, transcript, transcriptAt;
    protected String name, chrName;
    protected String mentorName = "", mentorNameChr = "";
    protected List<CurrentLecture> currentLectures = new ArrayList<CurrentLecture>(10);
    protected List<CustomMark> marks = new ArrayList<CustomMark>(100);


    public NotenApi(String username, String password) {
        this.username = username;
        this.password = password;
        this.reporter = new WirfAus();
    }

    public NotenApi(String username, String password, ExceptionAndEventHandler reporter) {
        this.username = username;
        this.password = password;
        this.reporter = reporter;
    }

    public ServerConnection connect() throws ModelException {
        final ServerConnection con;
        try {
            con = new ServerConnection("http://fh-noten.ha.bib.de", reporter, null);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
        con.connect(username, password, false);
        ConnectionIndex.getTheConnectionIndex().setConnection(reporter, con);
        this.con = con;
        return con;
    }

    public void initalize() throws ModelException {
        for (ServiceView sv : con.getServerView().getServices()) {
            ExtractStudentVisitor esv = new ExtractStudentVisitor();
            sv.accept(esv);
            if (esv.getStudentView() == null) {
                continue;
            }
            this.ssv = esv.getStudentView();
            this.ssv.connectStudentService(con, reporter);
            this.stud = ssv.getStudent();
        }
    }

    public void fetchMarks() throws ModelException {
        ProgrammeWithMarksView pmwmv = stud.getMarks();
        MarkProcessor processor = new MarkProcessor(pmwmv);
        this.processor = processor;
        List<CustomMark> result = processor.processMarks();
        this.programTitle = processor.getProgrammeName();
        this.marks.addAll(result);
    }

    public void fetchLectures() throws ModelException {
        Vector<LectureView> lectures = stud.getCurrentLectures().getCurrentLectures();
        lectures.forEach((lectureView -> {
            try {
                List<String> lecturers = new ArrayList<>(lectureView.getLecturers().size());
                for (PersonAsLecturerView lecturer : lectureView.getLecturers()) {
                    lecturers.add(lecturer.getPerson().getChrName() + " " + lecturer.getPerson().getName());
                }
                String planning = lectureView.getWithIndividualPlanningMessage();
                final String[] name = new String[1];
                lectureView.getWhat().accept(new ModuleOrUnitDirectVisitor() {
                    @Override
                    public void handleUnit(UnitView unitView) throws ModelException {
                        name[0] = unitView.getName();
                    }

                    @Override
                    public void handleAbstractModule(AbstractModuleView abstractModuleView) throws ModelException {
                        name[0] = abstractModuleView.getName();
                    }
                });
                this.currentLectures.add(new CurrentLecture(lecturers.toArray(new String[0]), name[0], planning));
            } catch (ModelException e) {
                e.printStackTrace();
            }
        }));
    }

    public void fetchPerson() throws ModelException {
        LightPersonView lpv = stud.getPerson();
        this.name = lpv.getName();
        this.chrName = lpv.getChrName();
       /* PersonAsLecturerView mav = stud.getMenteeAdmin().getMentor();
        if (mav != null) {
            this.mentorName = mav.getPerson().getName();
            this.mentorNameChr = mav.getPerson().getChrName();
        } */
    }

    public void fetchStats() throws ModelException {
        StudentServiceConnection ssc = this.ssv.connectStudentService(this.con, reporter);
        this.stats = ssc.fetchStudentStatistics(stud);
        ssc.refresherStop();
    }

    public void fetchTranscript() throws ModelException {
        StudentServiceConnection ssc = this.ssv.connectStudentService(this.con, reporter);
        getTranscript(ssc);
        ssc.refresherStop();
    }

    public void produceTranscript() throws ModelException {
        StudentServiceConnection ssc = this.ssv.connectStudentService(this.con, reporter);
        ssc.produceTranscript(stud);
        getTranscript(ssc);
        ssc.refresherStop();
    }

    private void getTranscript(StudentServiceConnection ssc) throws ModelException {
        final String[] transcript = new String[2];
        stud.getArchive().accept((DocumentArchiveVisitor) studentDocumentArchiveView -> {
            transcript[0] = ssc.produceDocument(studentDocumentArchiveView.getCurrentTransScript());


            Date generated = studentDocumentArchiveView.getCurrentTransScript().getGeneratedAt();
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            transcript[1] = sdf.format(generated);
        });
        if (transcript.length > 0 && transcript[0] != null) {
            this.transcript = transcript[0];
            if (transcript[1] != null) {
                this.transcriptAt = transcript[1];
            }
        }
        ssc.refresherStop();
    }

    public void fetchData() throws ModelException, InterruptedException {
        this.fetchMarks();
        this.fetchPerson();
        Thread.sleep(500); // avoid do not hack!
        this.fetchStats();
//        Thread.sleep(500); // avoid do not hack!
//        this.fetchTranscript();
    }


    public StudentView getStud() {
        return stud;
    }

    public String getEmail() throws ModelException {
        return stud.getEmail();
    }

    public String getMatrikelNo() throws ModelException {
        return stud.getMatrNo();
    }

    public String getGroupName() throws ModelException {
        return stud.getStudentGroupName();
    }

    public String getProgramTitle() {
        return programTitle;
    }

    public String getStats() {
        return stats;
    }

    public String getName() {
        return name;
    }

    public String getChrName() {
        return chrName;
    }

    public String getTranscript() {
        return transcript;
    }

    public String getTranscriptAt() {
        return transcriptAt;
    }

    public List<CustomMark> getMarks() {
        return marks;
    }

    public String getMentorName() {
        return mentorName;
    }

    public String getMentorNameChr() {
        return mentorNameChr;
    }

    public void stop() {
        this.con.refresherStop();
    }

    public String getUsername() {
        return username;
    }

    public List<CurrentLecture> getCurrentLectures() {
        return currentLectures;
    }
}
