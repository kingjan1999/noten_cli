package me.kingjan1999;

public class CurrentLecture {
    private String[] lecturers;
    private String name;
    private String planning;

    public CurrentLecture(String[] lecturers, String name, String planning) {
        this.lecturers = lecturers;
        this.name = name;
        this.planning = planning;
    }

    public String[] getLecturers() {
        return lecturers;
    }

    public String getName() {
        return name;
    }

    public String getPlanning() {
        return planning;
    }
}
