package me.kingjan1999;

import view.*;
import view.objects.WeightedMark;

import java.util.*;

public class MarkProcessor {
    private ProgrammeWithMarksView programmeWithMarksView;

    public MarkProcessor(ProgrammeWithMarksView programmeWithMarksView) {
        this.programmeWithMarksView = programmeWithMarksView;
    }

    public List<CustomMark> processMarks() throws ModelException {
        Vector<view.ModuleOrModuleGroupWithMarks> marks = this.programmeWithMarksView.getProgrammeMarks();
        List<CustomMark> result = new ArrayList<CustomMark>(marks.capacity() * 2);
        for (ModuleOrModuleGroupWithMarks mark : marks) {
            AsListMarkVisitor markVisitor = new AsListMarkVisitor();
            mark.accept(markVisitor);
            List<CustomMark> tempres = markVisitor.getMarks();
            result.addAll(markVisitor.getMarks());
        }
        return result;
    }

    public String getProgrammeName() {
        try {
            return this.programmeWithMarksView.getProgramme();
        } catch (ModelException e) {
            e.printStackTrace();
            return "Error";
        }
    }


    public static Map<String, String> markViewToStringMap(QualifiedMarkView mark) throws ModelException {
        Map<String, String> marks = new HashMap<String, String>(3);

        MarkToMarkVisitor mark1 = new MarkToMarkVisitor();
        mark.getMark1().accept(mark1);
        String mark_1 = mark1.getActualMark();
        marks.put("Note 1", mark_1);

        MarkToMarkVisitor mark2 = new MarkToMarkVisitor();
        mark.getMark2().accept(mark2);
        String mark_2 = mark2.getActualMark();
        marks.put("Note 2", mark_2);

        MarkToMarkVisitor mark3 = new MarkToMarkVisitor();
        mark.getMark3().accept(mark3);
        String mark_3 = mark3.getActualMark();
        marks.put("Note 3", mark_3);


        return marks;
    }

    public static Map<String, String> weightedMarkToStringMap(AbstarctPartWithMarkView view) throws ModelException {
        WeightedMarkView weighted = view.getWeightedMark();
        long actual_mark = weighted.getMark();
        Map<String, String> marks = new HashMap<String, String>(4);
        marks.put("Required cp", String.valueOf(weighted.getCpRequired()));
        marks.put("Given cp", String.valueOf(weighted.getCpGiven()));
        marks.put("Completely given cp", String.valueOf(weighted.getCpCompletelyGiven()));
        marks.put("No influence cp", String.valueOf(weighted.getCpNoInfluence()));
        return marks;
    }

}
