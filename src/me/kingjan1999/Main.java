package me.kingjan1999;

import org.apache.xmlrpc.XmlRpcException;
import org.json.JSONObject;
import view.MentorException;
import view.ModelException;
import viewClient.ServerConnection;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ModelException, XmlRpcException, IOException, MentorException, InterruptedException {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Benutzername: ");
        String username = scanner.nextLine();
        System.out.print("Passwort: ");
        String password = scanner.nextLine();
        System.out.println("\b\b\b\b\b");
        for (int i = 1; i < 15; i++) {
            System.out.println();
        }

        NotenApi notenApi = new NotenApi(username, password);
        ServerConnection con = notenApi.connect();
        notenApi.initalize();
        notenApi.fetchData();

        List<CustomMark> marks = notenApi.getMarks();
        String matrNo = notenApi.getMatrikelNo();
        String programName = notenApi.getProgramTitle();
        String groupName = notenApi.getGroupName();
        String email = notenApi.getEmail();

        JSONObject resultObject = new JSONObject();
        resultObject.put("marks", marks);
        resultObject.put("matrNo", matrNo);
        resultObject.put("programName", programName);
        resultObject.put("groupName", groupName);
        resultObject.put("email", email);
       // resultObject.put("stats", notenApi.getStats());
        resultObject.put("name", notenApi.getName());
        resultObject.put("chrName", notenApi.getChrName());
        resultObject.put("transcript", notenApi.getTranscript());
        resultObject.put("transcriptAt", notenApi.getTranscriptAt());
        System.out.println(resultObject.toString());

        con.refresherStop();
        System.exit(0);
    }

    public static String convertString(String s) {
        return s;
        /*
        try {
            //String neu = new String(s.getBytes("ISO-8859-1"), "UTF-8");
            //System.out.println("Transforming " + s + " to " + neu);
            return neu;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "<Error>";
        }*/
    }

}
