package me.kingjan1999;

import view.*;
import view.visitor.ServiceVisitor;

public class ExtractStudentVisitor implements ServiceVisitor {
    private StudentServiceView studentView = null;

    public StudentServiceView getStudentView() {
        return studentView;
    }

    @Override
    public void handleAdministrator(AdministratorView administratorView) throws ModelException {

    }

    @Override
    public void handleExaminationServiceReadOnly(ExaminationServiceReadOnlyView examinationServiceReadOnlyView) throws ModelException {

    }

    @Override
    public void handleFeedBackManager(FeedBackManagerView feedBackManagerView) throws ModelException {

    }

    @Override
    public void handleLecturer(LecturerView lecturerView) throws ModelException {

    }

    @Override
    public void handlePeriodServiceReadOnly(PeriodServiceReadOnlyView periodServiceReadOnlyView) throws ModelException {

    }

    @Override
    public void handleStudentFeedBack(StudentFeedBackView studentFeedBackView) throws ModelException {
        }

    @Override
    public void handleStudentService(StudentServiceView studentServiceView) throws ModelException {
        this.studentView = studentServiceView;
    }

    @Override
    public void handleExMailService(ExMailServiceView exMailServiceView) throws ModelException {

    }

    @Override
    public void handleMailerService(MailerServiceView mailerServiceView) throws ModelException {

    }

    @Override
    public void handleMentorService(MentorServiceView mentorServiceView) throws ModelException {

    }

    @Override
    public void handleProjectServer(ProjectServerView projectServerView) throws ModelException {

    }

    @Override
    public void handleProjectALServer(ProjectALServerView projectALServerView) throws ModelException {

    }

    @Override
    public void handleExaminationService(ExaminationServiceView examinationServiceView) throws ModelException {

    }

    @Override
    public void handleLecturerProjectServer(LecturerProjectServerView lecturerProjectServerView) throws ModelException {

    }

    @Override
    public void handleMailAdminService(MailAdminServiceView mailAdminServiceView) throws ModelException {

    }

    @Override
    public void handlePeriodService(PeriodServiceView periodServiceView) throws ModelException {

    }

    @Override
    public void handleAdministration(AdministrationView administrationView) throws ModelException {

    }

    @Override
    public void handleReportServer(ReportServerView reportServerView) throws ModelException {

    }
}
