package me.kingjan1999;

import view.ModelException;
import view.UserException;
import viewClient.ConnectionMaster;
import viewClient.ExceptionAndEventHandler;

public class WirfAus implements ExceptionAndEventHandler {
    @Override
    public void handleException(ModelException paramModelException) {
        System.out.println("handleException");
        System.out.println(paramModelException.getMessage());
        System.out.println(paramModelException.getErrorNumber());

    }

    @Override
    public void handleUserException(UserException paramUserException) {
        System.out.println("handleUserException");

    }

    @Override
    public void handleRefresh() {
        System.out.println("handleRefresh");

    }

    @Override
    public void handleOKMessage(String paramString) {
        System.out.println("Handle ok");
        System.out.println(paramString);
    }

    @Override
    public void setConnection(ConnectionMaster paramConnectionMaster) {
        System.out.println("Set Connection");
    }

    @Override
    public void initializeConnection() {
        System.out.println("Initalize Connection");
    }
}
