package me.kingjan1999;

import view.*;
import view.visitor.MarkVisitor;

public class MarkToMarkVisitor implements MarkVisitor {

    private String actualMark = "Nicht besetzt";

    String getActualMark() {
        return actualMark;
    }

    @Override
    public void handleDelayed(DelayedView delayedView) throws ModelException {
        actualMark = "Delayed";
    }

    @Override
    public void handleFiveZero(FiveZeroView fiveZeroView) throws ModelException {
        actualMark = "5,0";
    }

    @Override
    public void handleFourZero(FourZeroView fourZeroView) throws ModelException {
        actualMark = "4,0";
    }

    @Override
    public void handleNoResult(NoResultView noResultView) throws ModelException {
        actualMark = "Kein Ergebnis";
    }

    @Override
    public void handleNoResultAfterDelayed(NoResultAfterDelayedView noResultAfterDelayedView) throws ModelException {
        actualMark = "No Result (after delayed)";
    }

    @Override
    public void handleNotChosen(NotChosenView notChosenView) throws ModelException {
        actualMark = "Nicht gewählt";
    }

    @Override
    public void handleNullMark(NullMarkView nullMarkView) throws ModelException {
        actualMark = "N/A";
    }

    @Override
    public void handleOneEight(OneEightView oneEightView) throws ModelException {
        actualMark = "1,8";
    }

    @Override
    public void handleOneFive(OneFiveView oneFiveView) throws ModelException {
        actualMark = "1,5";
    }

    @Override
    public void handleOneFour(OneFourView oneFourView) throws ModelException {
        actualMark = "1,4";
    }

    @Override
    public void handleOneNine(OneNineView oneNineView) throws ModelException {
        actualMark = "1,9";
    }

    @Override
    public void handleOneOne(OneOneView oneOneView) throws ModelException {
        actualMark = "1,1";
    }

    @Override
    public void handleOneSeven(OneSevenView oneSevenView) throws ModelException {
        actualMark = "1,7";
    }

    @Override
    public void handleOneSix(OneSixView oneSixView) throws ModelException {
        actualMark = "1,6";
    }

    @Override
    public void handleOneThree(OneThreeView oneThreeView) throws ModelException {
        actualMark = "1,3;";
    }

    @Override
    public void handleOneTwo(OneTwoView oneTwoView) throws ModelException {
        actualMark = "1,2";
    }

    @Override
    public void handleOneZero(OneZeroView oneZeroView) throws ModelException {
        actualMark = "1,0";
    }

    @Override
    public void handleThreeEight(ThreeEightView threeEightView) throws ModelException {
        actualMark = "3,8";
    }

    @Override
    public void handleThreeFive(ThreeFiveView threeFiveView) throws ModelException {
        actualMark = "3,5";
    }

    @Override
    public void handleThreeFour(ThreeFourView threeFourView) throws ModelException {
        actualMark = "3,4";
    }

    @Override
    public void handleThreeNine(ThreeNineView threeNineView) throws ModelException {
        actualMark = "3,9";
    }

    @Override
    public void handleThreeOne(ThreeOneView threeOneView) throws ModelException {
        actualMark = "3,1";
    }

    @Override
    public void handleThreeSeven(ThreeSevenView threeSevenView) throws ModelException {
        actualMark = "3,7";
    }

    @Override
    public void handleThreeSix(ThreeSixView threeSixView) throws ModelException {
        actualMark = "3,6";
    }

    @Override
    public void handleThreeThree(ThreeThreeView threeThreeView) throws ModelException {
        actualMark = "3,3";
    }

    @Override
    public void handleThreeTwo(ThreeTwoView threeTwoView) throws ModelException {
        actualMark = "3,2";
    }

    @Override
    public void handleThreeZero(ThreeZeroView threeZeroView) throws ModelException {
        actualMark = "3,0";
    }

    @Override
    public void handleTwoEight(TwoEightView twoEightView) throws ModelException {
        actualMark = "2,8";
    }

    @Override
    public void handleTwoFive(TwoFiveView twoFiveView) throws ModelException {
        actualMark = "2,5";
    }

    @Override
    public void handleTwoFour(TwoFourView twoFourView) throws ModelException {
        actualMark = "2,4";
    }

    @Override
    public void handleTwoNine(TwoNineView twoNineView) throws ModelException {
        actualMark = "2,9";
    }

    @Override
    public void handleTwoOne(TwoOneView twoOneView) throws ModelException {
        actualMark = "2,1";
    }

    @Override
    public void handleTwoSeven(TwoSevenView twoSevenView) throws ModelException {
        actualMark = "2,7";
    }

    @Override
    public void handleTwoSix(TwoSixView twoSixView) throws ModelException {
        actualMark = "2,6";
    }

    @Override
    public void handleTwoThree(TwoThreeView twoThreeView) throws ModelException {
        actualMark = "2,3";
    }

    @Override
    public void handleTwoTwo(TwoTwoView twoTwoView) throws ModelException {
        actualMark = "2,2";
    }

    @Override
    public void handleTwoZero(TwoZeroView twoZeroView) throws ModelException {
        actualMark = "2,0";
    }

    @Override
    public void handleUnknown(UnknownView unknownView) throws ModelException {
        actualMark = "Unbekannt";
    }
}
