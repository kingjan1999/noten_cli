package me.kingjan1999;

import view.*;
import view.visitor.ModuleOrModuleGroupWithMarksVisitor;

import java.lang.reflect.Array;
import java.util.*;

public class AsListMarkVisitor implements ModuleOrModuleGroupWithMarksVisitor {

    private List<CustomMark> marks = new ArrayList<CustomMark>();
    private static int counter = 0;


    @Override
    public void handleAddOnModuleGroupWithMark(AddOnModuleGroupWithMarkView addOnModuleGroupWithMarkView) throws ModelException {
        this.handleModuleGroupWithMark(addOnModuleGroupWithMarkView);
    }

    @Override
    public void handleChoiceModuleGroupWithMark(ChoiceModuleGroupWithMarkView choiceModuleGroupWithMarkView) throws ModelException {
        this.handleModuleGroupWithMark(choiceModuleGroupWithMarkView);
    }

    @Override
    public void handleModuleGroupWithMark(ModuleGroupWithMarkView moduleGroupWithMarkView) throws ModelException {
        String moduleGroup = Main.convertString(moduleGroupWithMarkView.getModuleGroup());
        List<CustomMark> marks = new ArrayList<CustomMark>();
        for (AbstractModuleWithMarksView markView : moduleGroupWithMarkView.getGroupMarks()) {
            AsListMarkVisitor visitor = new AsListMarkVisitor();
            markView.accept(visitor);
            marks.addAll(visitor.getMarks());
        }
        Map<String, String> mark = MarkProcessor.weightedMarkToStringMap(moduleGroupWithMarkView);

        this.marks.add(new CustomMark(moduleGroup, mark, marks));
    }

    @Override
    public void handleUnitWithMarks(UnitWithMarksView unitWithMarksView) throws ModelException {
        String bezeichnung = Main.convertString(unitWithMarksView.getUnit());
        Map<String, String> mark = MarkProcessor.markViewToStringMap(unitWithMarksView.getMark());
        mark.putAll(MarkProcessor.weightedMarkToStringMap(unitWithMarksView));
        CustomMark cmark = new CustomMark(bezeichnung, mark, new ArrayList<>(0));
        marks.add(cmark);
    }

    @Override
    public void handleModuleWithMarks(ModuleWithMarksView moduleWithMarksView) throws ModelException {
        String bezeichnung = Main.convertString(moduleWithMarksView.getModule());

        List<CustomMark> customMarks = new ArrayList<CustomMark>();
        for (UnitWithMarksView unit : moduleWithMarksView.getModuleMarks()) {
            AsListMarkVisitor visitor = new AsListMarkVisitor();
            unit.accept(visitor);
            customMarks.addAll(visitor.getMarks());
        }
        Map<String, String> mark = MarkProcessor.markViewToStringMap(moduleWithMarksView.getMark());
        mark.putAll(MarkProcessor.weightedMarkToStringMap(moduleWithMarksView));

        marks.add(new CustomMark(bezeichnung, mark, customMarks));
    }

    @Override
    public void handleProjectWithMarks(ProjectWithMarksView projectWithMarksView) throws ModelException {
        String title = Main.convertString(projectWithMarksView.getCurrentTitle());
        Map<String, String> map = MarkProcessor.markViewToStringMap(projectWithMarksView.getMark());
        map.putAll(MarkProcessor.weightedMarkToStringMap(projectWithMarksView));

        this.marks.add(new CustomMark("Projekt: " + title, map, new ArrayList<>(0)));

    }


    List<CustomMark> getMarks() {
        return marks;
    }
}
