package me.kingjan1999;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CustomMark {

    private String bezeichnung;
    private Map<String, String> mark;
    private List<CustomMark> subMarks;

    CustomMark(String bezeichnung, Map<String, String> mark, List<CustomMark> subMarks) {
        this.bezeichnung = bezeichnung;
        this.mark = mark;
        this.subMarks = subMarks;
    }

    @Override
    public String toString() {
        return "CustomMark{" +
                "bezeichnung='" + bezeichnung + '\'' +
                ", mark='" + Arrays.toString(mark.values().toArray()) + '\'' +
                ", subMarks=" + Arrays.toString(subMarks.toArray()) +
                '}';
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public Map<String, String> getMark() {
        return mark;
    }

    public List<CustomMark> getSubMarks() {
        return subMarks;
    }
}
